package dominio;

public class Persona {
	//Atributos de la clase
	private int idPersona;
	private String nombre;
	private static int contadorPersona;

	//Constructor de la clase
	public Persona(String nombre) {
		this.nombre = nombre;		
		//Incrementar el contador por cada objeto nuevo
		Persona.contadorPersona++;
		//Asignar el nuevo valor a la variable idPersona
		this.idPersona = Persona.contadorPersona;
	}
	
	//Metodos get y set de cada atributo
	public int getIdPersona() {
		return this.idPersona;
	}
	
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public static int getContadorPersona(){
		return contadorPersona;
	}
	
	public static void setContadorPersona(int aContadorPersona) {
		contadorPersona = aContadorPersona;
	}
	
	public String toString() {
		return "Persona [idPersona: " + this.idPersona + ", nombre: " 
				+ this.nombre + "]"; 
	}
	
}
