package proyectoCaja;
import java.lang.Math;

public class Tetraedro extends Figura {
	//Atributos dela clase
	private int lado;
	
	//Constructor vacio de la clase
	public Tetraedro() {
		super();
		this.lado = lado;
	}
	
	//Constructor con parametros
	public Tetraedro(int lado) {
		super();
		this.lado = lado;
	}
	
	//Metodos get y set
	public int getLado() {
		return this.lado = lado;
	}
	
	public void setAltura(int lado) {
		this.lado = lado;
	}
	
	public double calcularVolumen() {
		return Math.sqrt(2)*Math.pow(lado,3)/12;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Tetraedro Lado: ").append(lado);
		return sb.toString();
	}
	
}
