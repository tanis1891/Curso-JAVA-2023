package proyectoCaja;
import java.lang.Math;

public class Cilindro extends Figura {
	//Atributos de la clase
	private double radio;
	private double altura;
	
	//Constructor de la clase
	public Cilindro() {
		super();
		this.radio = 0.0;
		this.altura = 0.0;
	}
	
	//Constructor con parametros
	public Cilindro(double radio, double altura) {
		super();
		this.radio = radio;
		this.altura = altura;
	}
	
	//Metodos set y get
	public double getRadio() {
		return this.radio;
	}
	
	public void setRadio(double radio) {
		this.radio = radio;
	}
	
	public double getAltura() {
		return this.altura;
	}
	
	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	//Metodo calcular volumen
	public double calcularVolumen() {
		return (double)(Math.PI*radio*2*altura);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Cilindro Radio: ").append(radio);
		sb.append(", Altura: ").append(altura);
		return sb.toString();
	}
}

