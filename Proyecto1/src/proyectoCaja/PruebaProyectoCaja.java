package proyectoCaja;

import java.util.concurrent.CancellationException;

public class PruebaProyectoCaja {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*ProyectoCaja proyectoCaja1 = new ProyectoCaja(5, 1, 9);
		Esfera esfera = new Esfera(2);
		Cilindro cilindro = new Cilindro(3,5);
		Tetraedro tetraedro = new Tetraedro(3); 
		
		int volumen = proyectoCaja1.calcularVolumenArgumentos(5, 1, 9);
		System.out.println("Resultado volumen: " + volumen);
		
		System.out.println("Resultado 2: " + proyectoCaja1.getAncho());
		
		//double volumenesfera = esfera.calcularVolumen()
		
		System.out.println("Resultado Volumen esfera: " + esfera.calcularVolumen());
		System.out.println("Resultado Volumen cilindro: " + cilindro.calcularVolumen());
		System.out.println("Resultado Volumen tetraedro: " + tetraedro.calcularVolumen());*/
		
		int numeroFiguras = 4;
		Figura[] arregloFigura = new Figura[numeroFiguras];
		
		//Instanciar arreglo de figuras
		for(int i=0; i<numeroFiguras; i++) {
			arregloFigura[i] = new Figura();
		}
		
		arregloFigura[0] = new Cilindro(3, 5);
		arregloFigura[1] = new Esfera(4);
		arregloFigura[2] = new Tetraedro(6);
		
		for(int i=0; i<numeroFiguras; i++) {
			System.out.print(arregloFigura[i]);
			System.out.println(" Volumen: " + arregloFigura[i].calcularVolumen());
		}
	}

}


