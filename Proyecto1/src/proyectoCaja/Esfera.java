package proyectoCaja;
import java.lang.Math;

public class Esfera extends Figura {
	//Atributos de la clase
	private double radio;
	
	//Constructor de  la clase
	public Esfera() {
		super();
		this.radio = 0.0;
	}
	
	//Constructor con parametros
	public Esfera(double radio) {
		super();
		this.radio = radio;
	}
	
	//Metodos get y set
	public double getRadio() {
		return this.radio;
	}
	
	public void setRadio(double radio) {
		this.radio = radio;
	}
	
	public double calcularVolumen() {
		return (double)((4/3)*Math.PI*this.radio*3);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Esfera Radio: ").append(radio);
		return sb.toString();
	}
}
