package proyectoCaja;

public interface PropiedadesFigura {
	
	public double calcularVolumen();
	
	public String toString();

}
