package test;

import dominio.Persona;

public class PersonaPrueba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Persona persona1 = new Persona("Juan", 5000.00, false);
		Persona persona2 = new Persona("Silvana", 10000.00, false);
		
		/*Se puede  mandar llamar al metodo toString ya que el metodo println incluye toString
		lo manda llamar de manera automatica*/
		System.out.println("Persona 1: " + persona1 + "\n");
		
		//Con el metodo set modifica el nombre
		persona1.setNombre("Carlos");
		
		persona2.setNombre("Mario");
		persona2.setSueldo(30000.00);
		persona2.setEliminado(true);
		
		
		//Con el metodo get obtiene el nombre
		System.out.println("Persona 1 nombre: " + persona1.getNombre());
		System.out.println("Persona 1 sueldo: " + persona1.getSueldo());
		System.out.println("Persona 1 eliminado: " + persona1.isEliminado() + "\n");
		
		System.out.println("Persona 2 nombre: " + persona2.getNombre());
		System.out.println("Persona 2 sueldo: " + persona2.getSueldo());
		System.out.println("Persona 2 eliminado: " + persona2.isEliminado() + "\n");
		
		persona2.setNombre("Julia");
		persona2.setSueldo(10000.00);
		persona2.setEliminado(false);
		
		System.out.println("Persona 2 modifico nombre: " + persona2.getNombre());
		System.out.println("Persona 2 modifico sueldo: " + persona2.getSueldo());
		System.out.println("Persona 2 modifico eliminado: "+ persona2.isEliminado() + "\n");
		
		System.out.println("Persona 1: " + persona1.toString());
	}

}
