package test;

import dominio.Empleado;

import java.time.LocalDate;

import dominio.Cliente;

public class HerenciaPrueba {

	public static void main(String[] args) {
		Empleado empleado1 = new Empleado("Juan", 5000.0);
		Cliente cliente1 = new Cliente("Tanya", false);

		System.out.println("Empleado = " + empleado1);
		System.out.println("Cliente = "+ cliente1);
	}

}
