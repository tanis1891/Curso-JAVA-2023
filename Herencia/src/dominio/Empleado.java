package dominio;

public class Empleado extends Persona{
	//Atributos de la clase
	private int idEmpleado;
	private double sueldo;
	private static int contadorEmpleado;
	
	//Constructor de la clase vacio
	public Empleado() {
		this.idEmpleado = ++Empleado.contadorEmpleado;
	}
		
	//Constructor de la clase con dos argumentos
	public Empleado(String nombre, double sueldo) {
		//super(nombre);
		this();
		this.nombre = nombre;
		this.sueldo = sueldo;
	}
	
	//Metodos get y set de cada atributo
	public int getIdEmpleado() {
		return this.idEmpleado;
	}
	
	public double getSueldo() {
		return this.sueldo;
	}
	
	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Empleado[idEmpleado = ").append(idEmpleado);
		sb.append(", sueldo = ").append(sueldo);
		sb.append(",  ").append(super.toString());
		return sb.toString();

	}
	
	
}
