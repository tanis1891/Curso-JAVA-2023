package dominio;

import java.time.LocalDate;
import java.util.Date;

public class Cliente extends Persona {
	//Atribustos de la clase
	private int idCliente;
	private LocalDate fechaRegistro = LocalDate.now();
	private boolean vip;
	public static int contadorCliente;
	
	//Constructor de la clase
	public Cliente(String nombre, boolean vip) {
		super(nombre);
		this.idCliente = ++Cliente.contadorCliente;
		this.vip = vip;
	}
	
	//Metodos get y set de cada atributo
	public int getIdCliente() {
		return this.idCliente;
	}
	
	public LocalDate getFechaRegistro() {
		return this.fechaRegistro;
	}
	
	public void setFechaRegistro(LocalDate fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	public boolean isVip() {
		return this.vip;
	}
	
	public void setVip(boolean vip) {
		this.vip = vip;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Cliente [idCliente = ").append(idCliente);
		sb.append(", Fecha de Registro = ").append(fechaRegistro);
		sb.append(", Es cliente Vip? = ").append(vip);
		sb.append(", ").append(super.toString());
		return sb.toString();
	}
	
	

}
