package operaciones;

public class Operaciones {
	
	//Metodo estatico
	/*Sobrecarga de metodos porque existen dos metodos con el mismo nombre, pero reciben
	dos argumentos diferentes*/
	public static int sumar(int a, int b) {
		System.out.println("sumar(int a, int b)");
		return a + b;
	}
	
	public static double sumar(double a, double b) {
		System.out.println("sumar(double a, double b)");
		return a + b;
	}
	
	

}
