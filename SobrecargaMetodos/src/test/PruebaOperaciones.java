package test;

import operaciones.Operaciones;

public class PruebaOperaciones {

	public static void main(String[] args) {
		var resultado = Operaciones.sumar(5, 8);
		System.out.println("Resultado: " + resultado);

		var resultado2 = Operaciones.sumar(2, 9);
		System.out.println("Resultado: " + resultado2);
	}

}
